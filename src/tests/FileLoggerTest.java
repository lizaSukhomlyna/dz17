import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

class FileLoggerTest {


    @Test
    void debug() throws IOException {
        FileLoggerConfigurationLoader configuration = new FileLoggerConfigurationLoader();
        Logger fileLogger = new FileLogger(configuration.load((Path.of("src","tests", "resources", "configurationDEBUG.txt"))));
        fileLogger.debug("MessageDebugTest");
        String debugData = Files.readString(Path.of("src","tests", "resources", "DebugLog.txt"));
        List<String> lines= Arrays.asList(debugData.split("\n"));
        String lastLine = lines.get(lines.size()-1);
        System.out.println(lastLine);
        Assert.assertTrue(lastLine.contains("MessageDebugTest"));
        Assert.assertTrue(lastLine.contains("DEBUG"));
        Assert.assertFalse(lastLine.contains("INFO"));
    }

    @Test
    void info() throws IOException {
        FileLoggerConfigurationLoader configuration = new FileLoggerConfigurationLoader();
        Logger fileLogger = new FileLogger(configuration.load(Path.of("src","tests", "resources", "configurationINFO.txt")));
        fileLogger.debug("MessageInfoTest");
        String debugData = Files.readString(Path.of("src","tests", "resources", "DebugLog.txt"));
        List<String> lines= Arrays.asList(debugData.split("\n"));
        String lastLine = lines.get(lines.size()-1);
        System.out.println(lastLine);
        Assert.assertTrue(lastLine.contains("MessageInfoTest"));
        Assert.assertTrue(lastLine.contains("INFO"));
        Assert.assertFalse(lastLine.contains("DEBUG"));
    }
}