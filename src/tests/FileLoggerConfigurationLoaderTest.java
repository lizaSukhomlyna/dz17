import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;

class FileLoggerConfigurationLoaderTest {

    @Test
    void load() throws IOException {
        FileLoggerConfigurationLoader configuration = new FileLoggerConfigurationLoader();
        FileLoggerConfiguration fileLoggerConfiguration = configuration.load((Path.of("src", "tests", "resources", "configurationDEBUG.txt")));
        System.out.println(fileLoggerConfiguration.getMaxsize());
        Assert.assertEquals("src/tests/resources/DebugLog.txt", fileLoggerConfiguration.getPathToWrite());
        Assert.assertEquals("[DATE][LOGGIN_LEVEL]  Сообщение- [MESSAGE]", fileLoggerConfiguration.getFormat());
        Assert.assertEquals(LoggingLevel.DEBUG, fileLoggerConfiguration.getLevelLogging());
        Assert.assertEquals(900, fileLoggerConfiguration.getMaxsize());
    }
}