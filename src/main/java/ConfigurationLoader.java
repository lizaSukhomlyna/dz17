import java.io.IOException;
import java.nio.file.Path;

public interface ConfigurationLoader {
    LoggerConfiguration load(Path path) throws IOException;
}
