public class StdOutConfiguration implements LoggerConfiguration{
    private String pathToWrite;
    private LoggingLevel levelLogging;
    private int maxsize;
    private String format;


    public String getPathToWrite() {
        return pathToWrite;
    }

    public LoggingLevel getLevelLogging() {
        return levelLogging;
    }

    public int getMaxsize() {
        return maxsize;
    }
    public String getFormat() {
        return format;
    }

    public void setPathToWrite(String pathToWrite) {
        this.pathToWrite = pathToWrite;
    }

    public void setLevelLogging(LoggingLevel levelLogging) {
        this.levelLogging = levelLogging;
    }

    public void setMaxsize(int maxsize) {
        this.maxsize = maxsize;
    }

    public void setFormat(String format) {
        this.format = format;
    }


}
