import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileLogger implements Logger{
    private Path loggingFile;
    private FileLoggerConfiguration fileLoggerConfiguration;


    public FileLogger(FileLoggerConfiguration fileLoggerConfiguration) throws IOException {
        Path loggingFile = Path.of(fileLoggerConfiguration.getPathToWrite());
        File f = new File(loggingFile.toString());
        if (!f.exists()) {
            Files.createFile(loggingFile);
        }
        this.loggingFile = loggingFile;
        this.fileLoggerConfiguration = fileLoggerConfiguration;
    }


    public void debug(String message) throws IOException {
        doRecord(message);
    }

    public void info(String message) throws IOException {
        doRecord(message);
    }

    private void doRecord(String message) throws IOException {
        int maxsize = this.fileLoggerConfiguration.getMaxsize();
        if ((Files.size(this.loggingFile) + message.length()) <= maxsize) {
            Date date = new Date();
            String format=this.fileLoggerConfiguration.getFormat();
            String outMessageWithActualDate= format.replace("DATE",date.toString());
            String outMessageWithActualLevel= outMessageWithActualDate.replace("LOGGIN_LEVEL",this.fileLoggerConfiguration.getLevelLogging().toString());
            String outMessage= outMessageWithActualLevel.replace("MESSAGE",message)+"\n";
            Files.write(this.loggingFile, outMessage.getBytes(), StandardOpenOption.APPEND);

        } else {
            Date date = new Date();
            SimpleDateFormat formatForDateNow = new SimpleDateFormat("yyyy.MM.dd_hh-mm-ss");
            Files.createFile(Path.of("src/resources", "Log" + formatForDateNow.format(date) + ".txt"));
            throw new FileMaxSizeReachedException("Max size is:" + maxsize + "." + "Now file on path: " + this.loggingFile + " have size:" + maxsize);
        }

    }


}
