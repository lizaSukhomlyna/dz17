import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class FileLoggerConfigurationLoader implements ConfigurationLoader {

    public FileLoggerConfiguration load(Path path) throws IOException {
        List<String> configurationData = Files.readAllLines(path);
        FileLoggerConfiguration configuration = new FileLoggerConfiguration();
        String[] splitData = new String[9];
        int count = 0;
        for (String s : configurationData) {
            System.out.println(s.split(": ")[1]);
            splitData[count] = s.split(": ")[1];
            count++;
        }
        configuration.setPathToWrite(splitData[0]);
        configuration.setLevelLogging(LoggingLevel.valueOf(splitData[1]));
        configuration.setMaxsize(Integer.parseInt(splitData[2]));
        configuration.setFormat(splitData[3]);

        return configuration;
    }


}
