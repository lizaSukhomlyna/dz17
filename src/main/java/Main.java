import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;


public class Main {
    public static void main(String[] args) throws IOException {

        FileLoggerConfigurationLoader configuration = new FileLoggerConfigurationLoader();
        LoggerConfiguration firstFileLogger = configuration.load(Path.of("src","main", "resources", "configuration.txt"));
        Logger fileLogger = new FileLogger(configuration.load(Path.of("src","main", "resources", "configuration.txt")));
        switch (firstFileLogger.getLevelLogging()) {
            case DEBUG:
                fileLogger.debug("Hello DEBUG");
                fileLogger.info("Hello INFO");
                break;
            case INFO:
                fileLogger.info("Hello ONLY INFO");
        }

    }


}
