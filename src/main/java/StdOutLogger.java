import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StdOutLogger implements Logger{
    private StdOutConfiguration configuration;


    public StdOutLogger(StdOutConfiguration configuration) {
        this.configuration = configuration;
    }

    public void debug(String message){
        doRecord(message);
    }

    public void info(String message){
        doRecord(message);
    }

    private void doRecord(String message){
        int maxsize = this.configuration.getMaxsize();
        Date date = new Date();
        String format=this.configuration.getFormat();
        String outMessageWithActualDate= format.replace("DATE",date.toString());
        String outMessageWithActualLevel= outMessageWithActualDate.replace("LOGGIN_LEVEL",this.configuration.getLevelLogging().toString());
        String outMessage= outMessageWithActualLevel.replace("MESSAGE",message);
        if (outMessage.length() <= maxsize) {
            System.out.println(outMessage);

        } else {
            SimpleDateFormat formatForDateNow = new SimpleDateFormat("yyyy.MM.dd_hh-mm-ss");
            System.out.println("Size error" +formatForDateNow.format(date));
            throw new FileMaxSizeReachedException("Max message is:" + maxsize + "." + "Now message have size:" + maxsize);
        }

    }


}
